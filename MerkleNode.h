/*! -------------------------------------------------------------------------*\
|   Merkle Node implementation
|   @see
https://www.codeproject.com/Articles/1176140/Understanding-Merkle-Trees-Why-use-them-who-uses-t
\*---------------------------------------------------------------------------*/
#ifndef MERKLETREE_MERKLENODE_H
#define MERKLETREE_MERKLENODE_H

#include "MerkleHash.h"
#include "TransactionSequence.h"

#include <iostream>
#include <string>

//! @struct MerkleNode
struct MerkleNode
{
    MerkleNode()  = delete;
    ~MerkleNode() = default;

    //! @fn     MerkleNode::MerkleTransaction
    //! brief   Named constructor for a transaction (leaf) node
    static MerkleNode* MerkleTransaction(const std::string txid)
    {
        return new MerkleNode(txid);
    }

    //! @fn     MerkleNode::MerkleParent
    //! @brief  Named constructor for a parent (intermediate) node
    static MerkleNode* MerkleParent(MerkleNode* left, MerkleNode* right)
    {
        return new MerkleNode(left, right);
    }

    //! @fn     MerkleNode::MerkleNode
    //! @brief  Constructor for a leaf (transaction) node
    MerkleNode(const std::string txid)
        : m_left(nullptr)
        , m_right(nullptr)
        , m_hash(txid)
        , m_txid(txid)
        , m_seqid(TransactionSequence::GetInstance().TransactionSID())
    {
    }

    //! @fn     MerkleNode::MerkleNode
    //! @brief  Constructor for a parent node
    MerkleNode(MerkleNode* left, MerkleNode* right)
        : m_left(left)
        , m_right(right)
        , m_hash()
        , m_txid()
    {
        std::string concat;
        if (m_left != nullptr)
        {
            concat  = m_left->GetHash();
            m_seqid = m_left->GetSequenceID();
        }

        if (m_right != nullptr)
        {
            concat += m_right->GetHash();
            m_seqid += m_right->GetSequenceID();
        }
        else
        {
            concat += m_left->GetHash();
            m_seqid += m_left->GetSequenceID();
        }

        m_hash.ComputeHash(concat);
    }

    //! @fn     MerkleNode::GetHash
    std::string GetHash() const
    {
        return m_hash.Value();
    }

    //! @fn     MerkleNode::GetData
    std::string GetData() const
    {
        return m_txid;
    }

    //! @fn     MerkleNode::GetSequenceID()
    std::string GetSequenceID() const
    {
        return m_seqid;
    }

    //! @fn     MerkleNode::IsLeaf
    bool IsLeaf() const
    {
        return (m_left == nullptr && m_right == nullptr);
    }

    MerkleNode* m_left;
    MerkleNode* m_right;
    MerkleHash m_hash;
    std::string m_txid;
    std::string m_seqid;
};

#endif // MERKLETREE_MERKLENODE_H
