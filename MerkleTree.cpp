/*! -------------------------------------------------------------------------*\
|   Merkle Tree implementation
|   @see
https://www.codeproject.com/Articles/1176140/Understanding-Merkle-Trees-Why-use-them-who-uses-t
\*---------------------------------------------------------------------------*/
#include "MerkleTree.h"
#include "MerkleNode.h"

#include <queue>

//! @fn     MerkleTree::MerkleTree
MerkleTree::MerkleTree(const std::vector<std::string>& transactions)
    : m_root(nullptr)
{
    BuildTreeFromTransactions_(transactions);
}

//! @fn     MerkleTree::~MerkleTree
MerkleTree::~MerkleTree()
{
    DeleteTree_(m_root);
}

//! @fn     MerkleTree::GetTreeIndex()
std::vector<MerkleNode*> MerkleTree::GetTreeIndex()
{
    IndexTree_(m_root);
    return m_treeIndex;
}

//! @fn     MerkleTree::BuildTreeFromTransactions_
//! @brief  Builds a Merkle Tree from a vector of transactions
//! @note   PRIVATE
void MerkleTree::BuildTreeFromTransactions_(const std::vector<std::string>& transactions)
{
    std::queue<MerkleNode*> treenodes;

    // Create leaf nodes for transactions
    for (auto transaction : transactions)
    {
        MerkleNode* txnode = MerkleNode::MerkleTransaction(transaction);
        treenodes.push(txnode);
    }

    // Start hashing pairs of nodes
    while (treenodes.size() > 1)
    {
        MerkleNode* left = treenodes.front();
        treenodes.pop();

        MerkleNode* right = treenodes.front();
        treenodes.pop();

        MerkleNode* parent = MerkleNode::MerkleParent(left, right);

        // Handle odd node out
        if (treenodes.size() == 1)
        {
            left = treenodes.front();
            treenodes.pop();
            treenodes.push(parent);

            parent = MerkleNode::MerkleParent(left, nullptr);
            treenodes.push(parent);
        }
        else
        {
            treenodes.push(parent);
        }
    }

    m_root = treenodes.front();
}

//! @fn     MerkleTree::DeleteTree_
//! @brief  Performs post-order traversal to delete tree nodes
void MerkleTree::DeleteTree_(MerkleNode* inode)
{
    if (inode == nullptr)
    {
        return;
    }

    DeleteTree_(inode->m_left);
    DeleteTree_(inode->m_right);
    delete inode;
}

//! @fn     MerkleTree::IndexTree_
//! @brief  Performs in-order traversal to index the tree
void MerkleTree::IndexTree_(MerkleNode* inode)
{
    if (inode == nullptr)
    {
        return;
    }

    m_treeIndex.push_back(inode);
    IndexTree_(inode->m_right);
    IndexTree_(inode->m_left);
}
