/*! -------------------------------------------------------------------------*\
|   Merkle Tree implementation
|   @see
https://www.codeproject.com/Articles/1176140/Understanding-Merkle-Trees-Why-use-them-who-uses-t
\*---------------------------------------------------------------------------*/
#ifndef MERKLETREE_MERKLETREE_H
#define MERKLETREE_MERKLETREE_H

#include <string>
#include <vector>

class MerkleNode;

//! @class  MerkleTree
class MerkleTree
{
public:
    MerkleTree() = delete;
    MerkleTree(const std::vector<std::string>& transactions);
    ~MerkleTree();

    std::vector<MerkleNode*> GetTreeIndex();

private:
    void BuildTreeFromTransactions_(const std::vector<std::string>& transactions);
    void DeleteTree_(MerkleNode*);
    void IndexTree_(MerkleNode*);

    MerkleNode* m_root;
    std::vector<MerkleNode*> m_treeIndex;
};

#endif // MERKLETREE_MERKLETREE_H
