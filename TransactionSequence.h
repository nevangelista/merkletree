/*! -------------------------------------------------------------------------*\
|   Singleton for transaction sequence numbers
\*---------------------------------------------------------------------------*/
#ifndef MERKLETREE_TRANSACTIONSEQUENCE_H
#define MERKLETREE_TRANSACTIONSEQUENCE_H

#include <iostream>
#include <string>

//! @class  TransactionSequence
class TransactionSequence
{
public:
    //! @fn     TransactionSequence::GetInstance
    static TransactionSequence& GetInstance()
    {
        static TransactionSequence instance;

        return instance;
    }

    //! @fn     Reset
    void Reset()
    {
        m_sequence = 0;
    }

    //! @fn     TransactionSequence::TransactionID
    static uint32_t TransactionID()
    {
        return m_sequence++;
    }

    //! @fn     TransactionSequence::TransactionSID
    static std::string TransactionSID()
    {
        return Stringize_(TransactionID());
    }

private:
    TransactionSequence() = default;

    TransactionSequence(const TransactionSequence&) = delete;
    TransactionSequence& operator=(const TransactionSequence&) = delete;

    static std::string Stringize_(uint32_t seqID);

    static uint32_t m_sequence;
};

#endif // MERKLETREE_TRANSACTIONSEQUENCE_H
