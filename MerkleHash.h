/*! -------------------------------------------------------------------------*\
|   Hashes a std::string using OpenSSL MD5
\*---------------------------------------------------------------------------*/
#ifndef MERKELTREE_MERKLEHASH_H
#define MERKELTREE_MERKLEHASH_H

#include <openssl/evp.h>
#include <string>

constexpr uint32_t SHA256_HASH_LEN = 32;

//! @class MerkleHash
//! @brief  Computes a SHA-256 hash for a std::string
class MerkleHash
{
public:
    //! @fn     MerkleHash
    MerkleHash()
        : m_messageContext(EVP_MD_CTX_new())
    {
    }

    //! @fn     MerkleHash
    MerkleHash(const std::string& value)
        : m_messageContext(EVP_MD_CTX_new())
    {
        m_hash = Stringize(ComputeHash(value));
    }

    //! @fn     MerkleHash
    MerkleHash(unsigned char* value)
        : m_messageContext(EVP_MD_CTX_new())
    {
        m_hash = Stringize(ComputeHash(Stringize(value)));
    }

    //! @fn     MerkleHash
    MerkleHash(const MerkleHash& lhs, const MerkleHash& rhs)
        : m_messageContext(EVP_MD_CTX_new())
    {
        std::string concat{lhs.Value()};
        concat.append(rhs.Value());
        m_hash = Stringize(ComputeHash(concat));
    }

    //! @fn     ~MerkleHash
    ~MerkleHash()
    {
        EVP_MD_CTX_free(m_messageContext);
    }

    //! @fn     ComputeHash
    unsigned char* ComputeHash(const std::string& inStr);

    //! @fn     Value
    std::string Value() const
    {
        return m_hash;
    }

    //! @fn     Stringize
    std::string Stringize(unsigned char* hash);

private:
    EVP_MD_CTX* m_messageContext;
    uint32_t m_outputLength;
    unsigned char m_messageDigest[EVP_MAX_MD_SIZE];
    std::string m_hash;
};

#endif // MERKELTREE_MERKLEHASH_H
