/*! -------------------------------------------------------------------------*\
|   Unit tests for MerkleTree and related components
\*---------------------------------------------------------------------------*/
#include "MerkleTree.h"
#include "MerkleNode.h"

#include <gmock/gmock.h>

#include <array>
#include <string>
#include <vector>

//! @class  MerkleTest
struct MerkleTest : public ::testing::Test
{
    std::array<std::string, 26> natos{
        {"alfa",   "bravo",   "charlie", "delta",  "echo",   "foxtrot", "golf",
         "hotel",  "india",   "juliett", "kilo",   "lima",   "mike",    "november",
         "oscar",  "papa",    "quebec",  "romeo",  "sierra", "tango",   "uniform",
         "victor", "whiskey", "x-ray",   "yankee", "zulu"}};
};

//! @test   PassingEmptyStringToHashShouldReturnExpected
TEST_F(MerkleTest, PassingEmptyStringToHashShouldReturnExpected)
{
    std::string expected{"e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"};

    std::string inStr{""};
    MerkleHash mhash(inStr);

    EXPECT_EQ(expected, mhash.Value());
}

//! @test   LowerAndUpperCaseCharactersShouldHaveDifferentHashes
TEST_F(MerkleTest, LowerAndUpperCaseCharactersShouldHaveDifferentHashes)
{
    MerkleHash lchash(std::string("a"));
    MerkleHash uchash(std::string("A"));

    EXPECT_NE(lchash.Value(), uchash.Value());
}

//! @test   ConcatenatedHashShouldDifferFromEither
TEST_F(MerkleTest, ConcatenatedHashShouldDifferFromEither)
{
    MerkleHash lchash(std::string("a"));
    MerkleHash uchash(std::string("A"));

    MerkleHash concat(lchash, uchash);

    EXPECT_NE(concat.Value(), lchash.Value());
    EXPECT_NE(concat.Value(), uchash.Value());
}

//! @test   HashOfLeafNodeShouldMatchRawHash
TEST_F(MerkleTest, HashOfLeafNodeShouldMatchRawHash)
{
    MerkleHash alfa(natos[0]);

    MerkleNode alfaNode(natos[0]);

    EXPECT_EQ(alfa.Value(), alfaNode.GetHash());
}

//! @test   HashOfTreeWithOneEntryShouldMatchConcatenatedNodeHash
TEST_F(MerkleTest, HashOfTreeWithOneEntryShouldMatchConcatenatedNodeHash)
{
    std::unique_ptr<MerkleNode> alfaTx{MerkleNode::MerkleTransaction(natos[0])};

    std::unique_ptr<MerkleNode> root{MerkleNode::MerkleParent(alfaTx.get(), nullptr)};

    std::string combo{alfaTx->GetHash() + alfaTx->GetHash()};
    MerkleHash combi(combo);

    EXPECT_EQ(root->GetHash(), combi.Value());
}

//! @test   IndexOfThreeTransactionMerkleTreeShouldContainSixElements
TEST_F(MerkleTest, IndexOfThreeTransactionMerkleTreeShouldContainSixElements)
{
    std::vector<std::string> transactions(natos.begin(), natos.begin() + 3);

    TransactionSequence::GetInstance().Reset();
    MerkleTree threeElmTree(transactions);
    std::vector<MerkleNode*> index = threeElmTree.GetTreeIndex();
    EXPECT_EQ(6, index.size());
}

//! @fn     main
int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}