/*! -------------------------------------------------------------------------*\
|   Hashes a std::string using OpenSSL MD5
\*---------------------------------------------------------------------------*/
#include "MerkleHash.h"

//! @fn     MerkleHash::ComputeHash
//! @brief  Creates SHA-256 hash for a std::string
unsigned char* MerkleHash::ComputeHash(const std::string& inStr)
{
    EVP_DigestInit(m_messageContext, EVP_sha256());
    EVP_DigestUpdate(
        m_messageContext, reinterpret_cast<const void*>(inStr.c_str()), inStr.length());
    EVP_DigestFinal(m_messageContext, m_messageDigest, &m_outputLength);

    m_hash = Stringize(&m_messageDigest[0]);
    return &m_messageDigest[0];
}

//! @fn     MerkleHash::Stringize
//! @brief  Converts an unsigned char to a std::string
std::string MerkleHash::Stringize(unsigned char* hash)
{
    std::string convert;
    char boofer[SHA256_HASH_LEN];
    for (auto i = 0; i < SHA256_HASH_LEN; ++i)
    {
        sprintf(boofer, "%02x", hash[i]);
        convert.append(boofer);
    }

    return convert;
}