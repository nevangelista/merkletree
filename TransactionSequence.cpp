/*! -------------------------------------------------------------------------*\
|   Unit tests for MerkleTree and related components
\*---------------------------------------------------------------------------*/
#include "TransactionSequence.h"
#include <cstdint>

uint32_t TransactionSequence::m_sequence = 0;

//! @fn     TransactionSequence::Stringize_
//! @brief  Converts an integer SID to a std::string
std::string TransactionSequence::Stringize_(uint32_t seqID)
{
    char boofer[8];
    sprintf(boofer, "%02x", seqID);
    return std::string(boofer);
}
